module Main (main) where

import qualified DSA.List as L

import DSA.Prelude

main :: IO ()
main = do
    let xs = L.fromList [1 .. 3] :: L.List Int
        ys = L.fromList [4 .. 6]
        zs = L.fromList [7 .. 9]
    putStrLn $ show (xs <> ys) <> ", expect: [1,2,3,4,5,6]"
    putStrLn $ show (L.concat $ L.fromList [xs, ys, zs]) <> ", expect: [1,2,3,4,5,6,7,8,9]"
