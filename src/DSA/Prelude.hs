module DSA.Prelude (
    module Prelude,
    module Data.Function,
    module Data.Functor,
    module Control.Applicative,
    module Control.Monad,
    module Control.Arrow,
    module Data.Foldable,
    module Data.List,
) where

import Control.Applicative
import Control.Arrow
import Control.Monad
import Data.Foldable
import Data.Function
import Data.Functor
import Data.List (unfoldr)
import Prelude hiding (unzip)
