module DSA.ListZipper (
    ListZipper (..),
    left,
    right,
    addLeft,
    addRight,
    replace,
    modify,
) where

import DSA.List
import DSA.Prelude hiding (left, right)

data ListZipper a
    = ListZipper
    { listZipperLeft :: List a
    , listZipperFocus :: a
    , listZipperRight :: List a
    }
    deriving (Show, Read, Eq, Ord)

instance Functor ListZipper where
    fmap f (ListZipper l focus r) =
        ListZipper (f <$> l) (f focus) (f <$> r)

left :: (MonadPlus m) => ListZipper a -> m (ListZipper a)
left (ListZipper (l :> ls) x rs) = pure $ ListZipper ls l (x :> rs)
left _ = mzero

right :: (MonadPlus m) => ListZipper a -> m (ListZipper a)
right (ListZipper ls x (r :> rs)) = pure $ ListZipper (x :> ls) r rs
right _ = mzero

addLeft :: a -> ListZipper a -> ListZipper a
addLeft x lz@(ListZipper{listZipperLeft = ls}) =
    lz{listZipperLeft = x :> ls}

addRight :: a -> ListZipper a -> ListZipper a
addRight x lz@(ListZipper{listZipperRight = rs}) =
    lz{listZipperRight = x :> rs}

replace :: a -> ListZipper a -> ListZipper a
replace = modify . const

modify :: (a -> a) -> ListZipper a -> ListZipper a
modify f lz@(ListZipper{listZipperFocus = focus}) =
    lz{listZipperFocus = f focus}
