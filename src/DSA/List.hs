module DSA.List (
    List (..),
    DSA.List.concat,
    DSA.List.reverse,
    fromList,
) where

import DSA.Prelude

reverse :: List a -> List a
reverse = foldl (flip (:>)) Empty

concat :: List (List a) -> List a
concat = foldr (<>) Empty

fromList :: [a] -> List a
fromList = foldr (:>) Empty

infixr 5 :>
data List a = Empty | a :> List a
    deriving (Show, Read, Eq, Ord)

instance Foldable List where
    foldl _ i Empty = i
    foldl f i (x :> xs) = foldl f (f i x) xs

    foldr _ d Empty = d
    foldr f d (x :> xs) = f x $ foldr f d xs

instance Functor List where
    fmap _ Empty = Empty
    fmap f (x :> xs) = f x :> fmap f xs
instance Applicative List where
    pure = (:> Empty)
    Empty <*> _ = Empty
    _ <*> Empty = Empty
    (f :> fs) <*> xs = (f <$> xs) <> (fs <*> xs)

instance Monad List where
    -- Empty >>= _ = Empty
    -- (x :> xs) >>= f = f x <> (xs >>= f)
    xs >>= f = DSA.List.concat $ f <$> xs

instance Alternative List where
    empty = Empty
    Empty <|> ys = ys
    xs <|> _ = xs
instance MonadFail List where
    fail = const Empty

instance Semigroup (List a) where
    xs <> Empty = xs
    Empty <> ys = ys
    (x :> xs) <> ys = x :> (xs <> ys)
instance Monoid (List a) where
    mempty = Empty
